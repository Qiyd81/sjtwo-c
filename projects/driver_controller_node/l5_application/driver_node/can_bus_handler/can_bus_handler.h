#pragma once
void can_handler__transmit_heartbeat_10hz(void);
void can_handler__manage_mia_10hz(void);
void can_handler__handle_all_incoming_messages_10Hz(void);
void populate_signal_status_10Hz(void);
void can_handler__transmit_messages_10hz(void);
void can_handler__transmit_debug_msg_10hz(void);