#pragma once

#include "can_bus.h"

void can_bus__init(void);

void can_bus__handle_bus_off(void);
