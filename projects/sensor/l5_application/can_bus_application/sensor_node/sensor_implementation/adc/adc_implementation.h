#pragma once

void adc_implementation__initialization(void);

float adc_implementation__get_battery_voltage(void);
